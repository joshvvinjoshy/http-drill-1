const http = require('http');
const fs = require('fs');
const { uuid } = require('uuidv4');
const { stat } = require('fs/promises');
const host = 'localhost';
const port = process.env.PORT || 8000;
const server = http.createServer((request, response)=>{
    // if(request.method == 'GET'){
        // console.log(request.url);
        if(request.url == '/html'){
            response.writeHead(200, {"Content-Type" : "text/html"});
            fs.readFile('index.html', 'utf-8', (err,data)=>{
                if(err){
                    console.end("file not found");
                }
                else{
                    response.end(data);
                }
            })

        }
        else if(request.url == '/json'){
            response.writeHead(200);
            fs.readFile('slide.json', 'utf-8', (err,data)=>{
                if(err){
                    console.end("file not found");
                }
                else{
                    response.end(data);
                }
            })
        }
        else if(request.url == '/uuid'){
            response.writeHead(200);
            const uuid_data = uuid();
            // console.log(typeof(uuid_data))
            const uuid_json = {
                "uuid" : uuid_data
            }
            const data = JSON.stringify(uuid_json);
            response.end(data);
        }
        else if(request.url.includes('/status')){
            const status_str = request.url.slice(8);
            const status = Number(status_str);
            // /if(status_str[0] == '3'|| status_str[0] == '2'){
                response.writeHead(status, {"Content-Type" : "application/json"});
                response.end(status.toString())
            // }
            // else{
            //     response.writeHead()
            // }
        }
        else if(request.url.includes('/delay')){
            const del = request.url.slice(7);
            const del_num = Number(del);
            setTimeout(()=>{
                response.writeHead(200);
                response.end("success");
            }, del_num*1000);
        }
        // response.end("hello from server");
        // response.end(request.json());
        // console.log(request.url);
    // }
})
server.listen(port,()=>{
    console.log("server listening")
});